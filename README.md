﻿# Overview

## General Assumptions

* This is a microservice
* Whatever client is calling the APIs has separate access to the products API. Thus it is not needed to provide any APIs that list products or product types

## Architecture

I've structured this assignment as I would a microservice. For a microservice I believe that there is limited value in the traditional n-tier solution where there are separate class libraries for each layer.
A microservice will likely not have enough code to justify that level of separation.

The controllers play the coordination role that a service layer would typically, and the actual logic for calculating insurance is separated out into a domain logic class - InsuranceCalculator.

All queries, whether to a database or a external api, are contained in the repository layer.
This layer is responsible for fetching all information (sometimes from multiple sources) and combining it into domain models for use elsewhere in the application. This roughly follows a DDD approach where the repository layer is responsible for constructing an aggregate root - in this case a product is the root - which contains all information that a business layer might need.

## Error Handling and Fault Tolerance

I'm making the assumption that there is an error monitoring system in place. For instance DataDog, Azure App Insights, New Relic, etc. For this reason there are no global error handlers installed and exceptions aren't logged explicitly. There is logging in places where they may add value in tracking down problems. I would rely on one a dedicated error monitoring system (such as one of the previously mentioned) to track, aggregate and alert on errors.

The system attempts to be fault tolerant - there is a retry policy in place (with exponential backoff) for http calls to the  product service. When calculating insurance costs for multiple products, invalid products will be skipped (and an error logged) - this allows partial responses. A downstream system could decide if this is acceptable or not.

I've made use of CSharps nullable reference types to make NullReferenceExceptions easier to catch at compile time and appropriate null checks are in place (with unit testing).

There are probably some bugs left but I'm happy with the error handling and fault tolerance given the time I had.

## Tasks

### Task 1

The logic for calculating the insurance value was wrong due to incorrect logic in an if statement. I added unit tests (that initially were failing) for the bug, fixed the bug and verified the tests then passed

### Task 2

I attempted to follow the TDD method of refactoring here. The primary point of importance for me is that at any given point during the refactoring process I should be confident that either the unit tests or the code is correct.

These were (roughly) the steps I followed:

I started refactored the network calls into a repository layer and verified the existing tests still passed - I was confident my repo was working correctly

I then refactored all the logic into a service class - again verifying the tests passed

At this point I also manually tested the code in case there was anything missed in the unit tests. There were some additional unit test cases that were added at this point.

Now that I was satisfied the code was correct, I refactored the unit tests and increased coverage of the business logic to 100%

I then added unit tests for the repository layer by mocking HttpMessageHandler. I removed the existing mocked api - this tends to be fragile and annoying to work with. Mocking HttpMessageHandler is more reliable in my experience and offers nearly identical functionality.

### Task 3

It would be preferable for the products api to expose an endpoint that allows multiple products/product_types to be retrieved without retrieving all of them.
In the absence of that endpoint making multiple calls is preferable to retrieving all the data from another service.
Additional logic was added to cater for getting insurance costs for multiple items. The code for calculating the cost is shared between the cart logic and the single product logic. This makes maintenance (and testing) much simpler.

### Task 4

Added a new category name to the surcharge check. This was a straightforward change to the business logic. Unit tests were added

### Task 5

Added a fairly straightforward and simple controller for getting and setting product type surcharge. This is backed by EFCore using SqlServer (LocalDb in dev mode). A SQL database is almost certainly not needed for this. It's likely in production that a lighter weight (NoSQL) database could be used instead. Choosing SqlServer was simply the most convenient given the time constraints of the project.

I refactored the logic for insurance calculations into a dedicated InsuranceCalculator class and added the surcharge logic. Refactoring the code followed a similar process to task 2 and simplified the unit tests. It also highlighted some test cases that I'd missed - these were added. There are still some test cases that are missing, but there are enough to give me confidence in the code.

Concurrency is handled using row versioning built into EntityFramework.
The client must pass the current version to the update method. If an old version is passed (which would happen in the case of concurrent modification) then a http status code 409 will be returned.
