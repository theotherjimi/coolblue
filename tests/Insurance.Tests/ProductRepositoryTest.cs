﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Insurance.Api.Repository;
using Insurance.Api.Repository.Models;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Xunit;

namespace Insurance.Tests;

public class ProductRepositoryTest
{
    [Fact]
    public async Task GetProduct_GivenId_ReturnProduct()
    {
        // arrange
        var productDto = new ProductDto(735246, "AEG L8FB86ES", 699, 124);
        var productTypeDto = new ProductTypeDto(124, "Washing machines", true);
        var expected = new Product(productDto.Id, productDto.Name, productDto.SalesPrice,
            new ProductType(productTypeDto.Id, productTypeDto.Name, productTypeDto.CanBeInsured, 50));
        var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
        handlerMock
            .Protected()
            .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.Is<HttpRequestMessage>(
                    m => m.RequestUri.Equals(new Uri("http://localhost:5002/products/735246"))),
                ItExpr.IsAny<CancellationToken>()
            )
            .ReturnsAsync(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(productDto, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }))
            })
            .Verifiable();
        handlerMock
            .Protected()
            .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.Is<HttpRequestMessage>(
                    m => m.RequestUri.Equals(new Uri("http://localhost:5002/product_types/124"))),
                ItExpr.IsAny<CancellationToken>()
            )
            .ReturnsAsync(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(productTypeDto, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }))
            })
            .Verifiable();

        var httpClient = new HttpClient(handlerMock.Object)
        {
            BaseAddress = new Uri("http://localhost:5002")
        };
        var surchargeRepositoryMock = new Mock<ISurchargeRepository>();
        surchargeRepositoryMock.Setup(s => s.GetSurcharge(expected.ProductType.Id)).ReturnsAsync(
            new ProductTypeSurcharge(expected.ProductType!.Id, expected.ProductType.SurchargeAmount, ""));

        var productRepository = new ProductRepository(httpClient, surchargeRepositoryMock.Object,
            new Mock<ILogger<ProductRepository>>().Object);
        // act

        var actual = await productRepository.GetProduct(735246);
        // assert
        handlerMock.Verify();
        actual.Should().BeEquivalentTo(expected);
    }

    [Theory]
    [InlineData(new[] {735246})]
    [InlineData(new[] {735246, 735296})]
    public async Task GetProducts_ReturnFilteredProducts(int[] productIds)
    {
        // arrange
        var products = new List<ProductDto>
        {
            new(735246, "AEG L8FB86ES", 699, 124),
            new(735296, "Canon EOS 5D Mark IV Body", 2699, 35)
        };
        var productTypes = new List<ProductTypeDto>
        {
            new(124, "Washing machines", true),
            new(35, "SLR cameras", true)
        };
        var mappedProductTypes = productTypes.Select(p => new ProductType(p.Id, p.Name, p.CanBeInsured, 90));
        var selectedProducts = products.Where(p => productIds.Contains(p.Id)).ToList();
        var expected = selectedProducts.Select(p =>
                new Product(p.Id, p.Name, p.SalesPrice, mappedProductTypes.First(pT => p.ProductTypeId == pT.Id)))
            .ToList();
        var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
        foreach (var product in selectedProducts)
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.Is<HttpRequestMessage>(
                        m => m.RequestUri.Equals(new Uri($"http://localhost:5002/products/{product.Id}"))),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(product, new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }))
                })
                .Verifiable();

        foreach (var productType in productTypes.Where(pt =>
                     selectedProducts.Select(p => p.ProductTypeId).Contains(pt.Id)))
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.Is<HttpRequestMessage>(
                        m => m.RequestUri.Equals(new Uri($"http://localhost:5002/product_types/{productType.Id}"))),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(productType, new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }))
                })
                .Verifiable();

        var httpClient = new HttpClient(handlerMock.Object)
        {
            BaseAddress = new Uri("http://localhost:5002")
        };
        var surchargeRepositoryMock = new Mock<ISurchargeRepository>();
        foreach (var product in expected)
            surchargeRepositoryMock.Setup(s => s.GetSurcharge(product.ProductType.Id)).ReturnsAsync(
                new ProductTypeSurcharge(product.ProductType!.Id, product.ProductType.SurchargeAmount, ""));

        var productRepository = new ProductRepository(httpClient, surchargeRepositoryMock.Object,
            new Mock<ILogger<ProductRepository>>().Object);
        // act
        var actual = await productRepository.GetProducts(productIds);
        // assert
        handlerMock.Verify();
        actual.Should().BeEquivalentTo(expected);
    }
}