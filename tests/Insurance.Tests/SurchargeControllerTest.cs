﻿using System.Threading.Tasks;
using FluentAssertions;
using Insurance.Api.Controllers;
using Insurance.Api.Repository;
using Insurance.Api.Repository.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Moq;
using Xunit;

namespace Insurance.Tests;

public class SurchargeControllerTest
{
    [Fact]
    public async Task GetProductTypeSurcharge_GivenValidProductIdInput_ReturnsObject()
    {
        // arrange
        var mockSurchargeRepository = new Mock<ISurchargeRepository>();
        var productTypeSurcharge = new ProductTypeSurcharge(1, 10.0f, "1");
        mockSurchargeRepository.Setup(m => m.GetSurcharge(It.IsAny<int>()))
            .ReturnsAsync(productTypeSurcharge);

        var surchargeController = new SurchargeController(mockSurchargeRepository.Object);

        // act
        var actual = await surchargeController.GetProductTypeSurcharge(1);

        // assert
        var value = Assert.IsType<OkObjectResult>(actual);
        value.Value.Should().BeEquivalentTo(productTypeSurcharge);
    }

    [Fact]
    public async Task GetProductTypeSurcharge_GivenInvalidProductIdInput_Return404()
    {
        // arrange
        var mockSurchargeRepository = new Mock<ISurchargeRepository>();
        mockSurchargeRepository.Setup(m => m.GetSurcharge(It.IsAny<int>())).ReturnsAsync((ProductTypeSurcharge) null);

        var surchargeController = new SurchargeController(mockSurchargeRepository.Object);

        // act
        var actual = await surchargeController.GetProductTypeSurcharge(1);

        // assert
        Assert.IsType<NotFoundResult>(actual);
    }

    [Fact]
    public async Task SetProductTypeSurcharge_GivenValidInput_UpdatesRepository()
    {
        // arrange
        var mockSurchargeRepository = new Mock<ISurchargeRepository>();
        mockSurchargeRepository.Setup(m => m.UpdateSurcharge(It.IsAny<ProductTypeSurcharge>()))
            .Verifiable();

        var surchargeController = new SurchargeController(mockSurchargeRepository.Object);

        // act
        var actual = await surchargeController.SetProductTypeSurcharge(new ProductTypeSurcharge(1, 10, ""));

        // assert
        Assert.IsType<OkResult>(actual);
        mockSurchargeRepository.Verify();
    }

    [Fact]
    public async Task SetProductTypeSurcharge_GivenInvalidVersionInput_ReturnConflict()
    {
        // arrange
        var mockSurchargeRepository = new Mock<ISurchargeRepository>();
        mockSurchargeRepository.Setup(m => m.UpdateSurcharge(It.IsAny<ProductTypeSurcharge>()))
            .ThrowsAsync(new OptimisticConcurrencyException("Test Message"));

        var surchargeController = new SurchargeController(mockSurchargeRepository.Object);

        // act
        var actual = await surchargeController.SetProductTypeSurcharge(new ProductTypeSurcharge(1, 10, ""));

        // assert
        var conflict = Assert.IsType<ConflictObjectResult>(actual);
        var expected = new ModelStateDictionary();
        expected.AddModelError("Version", "Test Message");
        conflict.Value.Should().BeEquivalentTo(new ValidationProblemDetails(expected));
    }
}