﻿using System.Collections.Generic;
using FluentAssertions;
using Insurance.Api.Insurance;
using Insurance.Api.Repository.Models;
using Xunit;

namespace Insurance.Tests;

public class InsuranceCalculatorTests
{
    public static IEnumerable<object[]> TestData =>
        new List<object[]>
        {
            // normal insurance
            new object[] {new Product(1, "Test 1", 100, new ProductType(1, "Test 1", true, 0)), 1, 0},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", true, 0)), 1, 1000},
            new object[] {new Product(1, "Test 1", 1999, new ProductType(1, "Test 1", true, 0)), 1, 1000},
            new object[] {new Product(1, "Test 1", 2000, new ProductType(1, "Test 1", true, 0)), 1, 2000},
            new object[] {new Product(1, "Test 1", 5000, new ProductType(1, "Test 1", true, 0)), 1, 2000},
            // tests for special categories
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Laptops", true, 0)), 1, 1500},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Smartphones", true, 0)), 1, 1500},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Digital cameras", true, 0)), 1, 1500},
            // tests for surcharge
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", true, 200)), 1, 1200},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", true, 200)), 1, 1200},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", true, 200)), 1, 1200},
            // tests for special categories and surcharge
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Laptops", true, 200)), 1, 1700},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Smartphones", true, 200)), 1, 1700},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Digital cameras", true, 200)), 1, 1700},
            // multiple items in cart
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", true, 0)), 2, 2000},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", true, 800)), 2, 3600},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Laptops", true, 0)), 2, 3000},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Smartphones", true, 0)), 2, 3000},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Digital cameras", true, 0)), 2, 3000},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Laptops", true, 200)), 2, 3400},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Smartphones", true, 200)), 2, 3400},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Digital cameras", true, 200)), 2, 3400},
            // no insurance
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", false, 500)), 1, 0},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", false, 500)), 2, 0},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Laptops", false, 500)), 1, 0},
            new object[] {new Product(1, "Test 1", 500, new ProductType(1, "Test 1", false, 0)), 1, 0}
        };

    [Theory]
    [MemberData(nameof(TestData))]
    public void CalculateInsuranceForProduct(Product product, int count, float expected)
    {
        // arrange
        var calculator = new InsuranceCalculator();
        // act
        var actual = calculator.CalculateInsuranceForProduct(product, count);
        // Assert
        actual.Should().Be(expected);
    }
}