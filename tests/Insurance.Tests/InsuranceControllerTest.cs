﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Insurance.Api.Controllers;
using Insurance.Api.Controllers.Models;
using Insurance.Api.Insurance;
using Insurance.Api.Repository;
using Insurance.Api.Repository.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Insurance.Tests;

public class InsuranceControllerTest
{
    public static IEnumerable<object[]> InsuranceTestData =>
        new List<object[]>
        {
            new object[] {new InsuranceItemTestData(1, "Nothing", 350, true, 0)},
            new object[] {new InsuranceItemTestData(2, "Nothing", 550, true, 1000)},
            new object[] {new InsuranceItemTestData(2, "Laptops", 550, true, 1500)},
            new object[] {new InsuranceItemTestData(2, "Smartphones", 550, true, 1500)},
            new object[] {new InsuranceItemTestData(3, "Smartphones", 2500, true, 2500)},
            new object[] {new InsuranceItemTestData(3, "Digital cameras", 2700, true, 2500)},
            new object[] {new InsuranceItemTestData(3, "Nothing", 2500, true, 2000)},
            new object[] {new InsuranceItemTestData(3, "Nothing", 2500, false, 0)}
        };

    public static IEnumerable<object[]> CartInsuranceTestData =>
        new List<object[]>
        {
            new object[]
            {
                new List<CartInsuranceItemTestData>
                {
                    new(1, 1, "Nothing", 350, true, 0),
                    new(1, 2, "Nothing", 550, true, 1000)
                }
            },
            new object[]
            {
                new List<CartInsuranceItemTestData>
                {
                    new(1, 1, "Nothing", 350, true, 0),
                    new(1, 2, "Laptops", 550, true, 1500)
                }
            },
            new object[]
            {
                new List<CartInsuranceItemTestData>
                {
                    new(1, 1, "Smartphones", 350, true, 500),
                    new(1, 2, "Laptops", 550, true, 1500)
                }
            },
            new object[]
            {
                new List<CartInsuranceItemTestData>
                {
                    new(1, 1, "Nothing", 350, true, 0),
                    new(1, 2, "Nothing", 550, false, 0)
                }
            }
        };

    [Theory]
    [MemberData(nameof(InsuranceTestData))]
    public async Task CalculateInsurance_GivenInput(InsuranceItemTestData itemTestData)
    {
        // arrange
        var mockProductRepository = new Mock<IProductRepository>();
        var product = new Product(itemTestData.ProductId, $"Test {itemTestData.ProductId}",
            itemTestData.SalesPrice,
            new ProductType(itemTestData.ProductId, itemTestData.ProductTypeName, itemTestData.CanBeInsured, 0));
        mockProductRepository.Setup(m => m.GetProduct(itemTestData.ProductId)).ReturnsAsync(
            product);

        var mockInsuranceCalculator = new Mock<IInsuranceCalculator>();
        mockInsuranceCalculator.Setup(m => m.CalculateInsuranceForProduct(product, 1))
            .Returns(itemTestData.ExpectedInsuranceAmount);

        var insuranceController = new InsuranceController(mockProductRepository.Object, mockInsuranceCalculator.Object);

        // act
        var actual = await insuranceController.CalculateInsurance(new InsuranceRequestDto(itemTestData.ProductId));

        // assert
        var okObjectResult = Assert.IsType<OkObjectResult>(actual);
        okObjectResult.Value.Should()
            .BeEquivalentTo(new InsuranceDto(itemTestData.ProductId, itemTestData.ExpectedInsuranceAmount));
    }

    [Fact]
    public async Task CalculateInsurance_GivenInvalidProductIdInput_Return404()
    {
        // arrange
        var mockProductRepository = new Mock<IProductRepository>();
        mockProductRepository.Setup(m => m.GetProduct(It.IsAny<int>())).ReturnsAsync((Product) null);

        var insuranceController =
            new InsuranceController(mockProductRepository.Object, new Mock<IInsuranceCalculator>().Object);

        // act
        var actual = await insuranceController.CalculateInsurance(new InsuranceRequestDto(1));

        // assert
        Assert.IsType<NotFoundResult>(actual);
    }

    [Theory]
    [MemberData(nameof(CartInsuranceTestData))]
    public async Task CalculateCartInsurance_GivenInput(List<CartInsuranceItemTestData> itemTestDataList)
    {
        // arrange
        var mockProductRepository = new Mock<IProductRepository>();
        var products = itemTestDataList.Select(i => new Product(i.ProductId, $"Test {i.ProductId}",
                i.SalesPrice, new ProductType(i.ProductId, i.ProductTypeName, i.CanBeInsured, 0)))
            .ToList();
        mockProductRepository.Setup(m => m.GetProducts(It.IsAny<IEnumerable<int>>())).ReturnsAsync(
            products
        );
        var mockInsuranceCalculator = new Mock<IInsuranceCalculator>();
        foreach (var product in products)
            mockInsuranceCalculator.Setup(m => m.CalculateInsuranceForProduct(product, 1))
                .Returns(itemTestDataList.First(x => x.ProductId == product.Id).ExpectedInsuranceAmount);

        var insuranceController = new InsuranceController(mockProductRepository.Object, mockInsuranceCalculator.Object);
        var cartInsuranceRequestDto =
            new CartInsuranceRequestDto(itemTestDataList.Select(i => new CartProductItemDto(i.Count, i.ProductId)));
        var expected = new CartInsuranceDto(
            itemTestDataList.Select(i => new InsuranceDto(i.ProductId, i.ExpectedInsuranceAmount)),
            itemTestDataList.Sum(i => i.ExpectedInsuranceAmount));

        // act
        var actual = await insuranceController.CalculateCartInsurance(cartInsuranceRequestDto);

        // assert
        var okObjectResult = Assert.IsType<OkObjectResult>(actual);
        okObjectResult.Value.Should().BeEquivalentTo(expected);
    }

    public record InsuranceItemTestData(int ProductId, string ProductTypeName, int SalesPrice, bool CanBeInsured,
        int ExpectedInsuranceAmount);

    public record CartInsuranceItemTestData(int Count, int ProductId, string ProductTypeName, int SalesPrice,
        bool CanBeInsured, int ExpectedInsuranceAmount);
}