using System;
using System.Net.Http;
using Insurance.Api.Db;
using Insurance.Api.Insurance;
using Insurance.Api.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;
using Polly.Contrib.WaitAndRetry;
using Polly.Extensions.Http;
using Polly.RateLimit;
using Polly.Timeout;

namespace Insurance.Api;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        services.AddScoped<IInsuranceCalculator, InsuranceCalculator>();
        services.AddScoped<ISurchargeRepository, SurchargeRepository>();
        services.AddDbContext<InsuranceContext>(options =>
            options.UseSqlServer(
                Configuration["SqlConnectionString"]));

        // retry failed request 5 times with exponential backoff
        var retryPolicy = HttpPolicyExtensions
            .HandleTransientHttpError()
            .Or<TimeoutRejectedException>()
            .Or<RateLimitRejectedException>()
            .WaitAndRetryAsync(Backoff.DecorrelatedJitterBackoffV2(TimeSpan.FromSeconds(1), 3, fastFirst: true));

        // requests timeout after 3 seconds
        var timeoutPolicy = Policy.TimeoutAsync<HttpResponseMessage>(3);

        // rate limit requests to 50 per 10 seconds
        var rateLimitPolicy = Policy
            .RateLimitAsync<HttpResponseMessage>(50, TimeSpan.FromSeconds(10), 15);
        
        // 5 consecutive exceptions will trip the circuit breaker
        var circuitBreaker = HttpPolicyExtensions
            .HandleTransientHttpError()
            .CircuitBreakerAsync<HttpResponseMessage>(
                5, TimeSpan.FromSeconds(30)
            );
        
        services.AddHttpClient<IProductRepository, ProductRepository>()
            .ConfigureHttpClient(client => { client.BaseAddress = new Uri(Configuration["ProductApi"]); })
            .AddPolicyHandler(retryPolicy)
            .AddPolicyHandler(rateLimitPolicy)
            .AddPolicyHandler(circuitBreaker)
            .AddPolicyHandler(timeoutPolicy);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseAuthorization();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}