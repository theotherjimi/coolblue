﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance.Api.Db;
using Insurance.Api.Repository.Models;
using Microsoft.EntityFrameworkCore;

namespace Insurance.Api.Repository;

public class SurchargeRepository : ISurchargeRepository
{
    private readonly InsuranceContext _insuranceContext;

    public SurchargeRepository(InsuranceContext insuranceContext)
    {
        _insuranceContext = insuranceContext;
    }

    public async Task<ProductTypeSurcharge?> GetSurcharge(int productTypeId)
    {
        var entity = await _insuranceContext
            .ProductTypeSurcharges
            .FirstOrDefaultAsync(p => p.ProductTypeId == productTypeId);
        if (entity == null) return null;
        return new ProductTypeSurcharge(entity.ProductTypeId, entity.Surcharge / 100.0f,
            Convert.ToHexString(entity.RowVersion));
    }

    public async Task<IEnumerable<ProductTypeSurcharge>> GetSurcharges(IEnumerable<int> productTypeIds)
    {
        var entities = await _insuranceContext
            .ProductTypeSurcharges
            .Where(p => productTypeIds.Contains(p.ProductTypeId))
            .ToListAsync();
        return entities.Select(entity => new ProductTypeSurcharge(entity.ProductTypeId, entity.Surcharge / 100.0f,
            Convert.ToHexString(entity.RowVersion)));
    }

    public async Task UpdateSurcharge(ProductTypeSurcharge productTypeSurcharge)
    {
        var entity = await _insuranceContext
            .ProductTypeSurcharges
            .FirstOrDefaultAsync(p => p.ProductTypeId == productTypeSurcharge.ProductTypeId);
        if (entity != null)
        {
            _insuranceContext.Entry(entity).Property("RowVersion").OriginalValue =
                Convert.FromHexString(productTypeSurcharge.Version);
        }
        else
        {
            entity = new ProductTypeSurchargeEntity();
            _insuranceContext.Add(entity);
        }

        entity.ProductTypeId = productTypeSurcharge.ProductTypeId;
        entity.Surcharge = (int) (productTypeSurcharge.SurchargeAmount * 100);
        try
        {
            await _insuranceContext.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            throw new OptimisticConcurrencyException("Conflict when updating surcharge amount. Try again");
        }
    }
}