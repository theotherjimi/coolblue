﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Insurance.Api.Repository.Models;

namespace Insurance.Api.Repository;

public interface ISurchargeRepository
{
    public Task<ProductTypeSurcharge?> GetSurcharge(int productTypeId);
    public Task<IEnumerable<ProductTypeSurcharge>> GetSurcharges(IEnumerable<int> productTypeIds);
    public Task UpdateSurcharge(ProductTypeSurcharge productTypeSurcharge);
}