﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Insurance.Api.Repository.Models;

namespace Insurance.Api.Repository;

public interface IProductRepository
{
    public Task<Product?> GetProduct(int id);
    public Task<IEnumerable<Product>> GetProducts(IEnumerable<int> productIds);
}