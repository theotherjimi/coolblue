﻿using System.ComponentModel.DataAnnotations;

namespace Insurance.Api.Repository.Models;

public record ProductTypeSurcharge(
    [Required] [Range(0, int.MaxValue, ErrorMessage = "Product Type Id  must be a positive integer")]
    int ProductTypeId,
    [Required] [Range(0, float.MaxValue, ErrorMessage = "Surcharge must be positive")]
    float SurchargeAmount, string Version);