﻿namespace Insurance.Api.Repository.Models;

public record ProductType(int Id, string Name, bool CanBeInsured, float SurchargeAmount);