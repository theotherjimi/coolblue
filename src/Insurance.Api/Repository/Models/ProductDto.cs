﻿namespace Insurance.Api.Repository.Models;

public record ProductDto(int Id, string Name, float SalesPrice, int ProductTypeId);