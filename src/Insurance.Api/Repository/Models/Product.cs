﻿namespace Insurance.Api.Repository.Models;

public record Product(int Id, string Name, float SalesPrice, ProductType? ProductType);