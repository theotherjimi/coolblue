﻿namespace Insurance.Api.Repository.Models;

public record ProductTypeDto(int Id, string Name, bool CanBeInsured);