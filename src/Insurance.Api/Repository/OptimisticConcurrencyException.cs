﻿using System;

namespace Insurance.Api.Repository;

public class OptimisticConcurrencyException : Exception
{
    public OptimisticConcurrencyException(string message) : base(message)
    {
    }
}