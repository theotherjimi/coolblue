﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Insurance.Api.Repository.Models;
using Microsoft.Extensions.Logging;

namespace Insurance.Api.Repository;

public class ProductRepository : IProductRepository
{
    private readonly HttpClient _client;
    private readonly ILogger<ProductRepository> _logger;
    private readonly ISurchargeRepository _surchargeRepository;

    public ProductRepository(HttpClient client, ISurchargeRepository surchargeRepository,
        ILogger<ProductRepository> logger)
    {
        _client = client;
        _surchargeRepository = surchargeRepository;
        _logger = logger;
    }

    public async Task<Product?> GetProduct(int id)
    {
        var result = await _client.GetAsync($"/products/{id:D}");
        if (!result.IsSuccessStatusCode) return null;
        var dto = await result.Content.ReadFromJsonAsync<ProductDto>();
        if (dto is null)
        {
            _logger.LogWarning("Product is missing for id {Product}", id);
            return null;
        }

        var productType = await GetProductType(dto.ProductTypeId);
        if (productType is null) _logger.LogError("Product Type is missing for product {Product}", dto.Id);
        return new Product(dto.Id, dto.Name, dto.SalesPrice, productType);
    }

    public async Task<IEnumerable<Product>> GetProducts(IEnumerable<int> productIds)
    {
        var products = new List<Product>();
        foreach (var productId in productIds)
        {
            var product = await GetProduct(productId);
            if (product is not null)
                products.Add(product);
            else
                _logger.LogWarning("Could not retrieve product {ProductId}", productId);
        }

        return products;
    }

    private async Task<ProductType?> GetProductType(int id)
    {
        var result = await _client.GetAsync($"/product_types/{id:D}");
        if (!result.IsSuccessStatusCode) return null;
        var dto = await result.Content.ReadFromJsonAsync<ProductTypeDto>();
        if (dto == null) return null;
        var surcharge = await _surchargeRepository.GetSurcharge(dto.Id);
        return new ProductType(dto.Id, dto.Name, dto.CanBeInsured, surcharge?.SurchargeAmount ?? 0);
    }
}