﻿namespace Insurance.Api.Controllers.Models;

public record InsuranceDto(int ProductId, float InsuranceValue);