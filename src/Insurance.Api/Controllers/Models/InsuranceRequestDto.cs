﻿using System.ComponentModel.DataAnnotations;

namespace Insurance.Api.Controllers.Models;

public record InsuranceRequestDto(
    [Required] [Range(0, int.MaxValue, ErrorMessage = "Product Id  must be a positive integer")]
    int ProductId);