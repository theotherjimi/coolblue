﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Insurance.Api.Controllers.Models;

public record CartInsuranceRequestDto([Required] IEnumerable<CartProductItemDto> CartItems);