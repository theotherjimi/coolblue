﻿namespace Insurance.Api.Controllers.Models;

public record Error(string Message);