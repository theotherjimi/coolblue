﻿using System.ComponentModel.DataAnnotations;

namespace Insurance.Api.Controllers.Models;

public record CartProductItemDto(
    [Required] [Range(1, int.MaxValue, ErrorMessage = "Count must be greater than 0")]
    int Count,
    [Required] [Range(0, int.MaxValue, ErrorMessage = "Product Id  must be a positive integer")]
    int ProductId);