﻿using System.Collections.Generic;

namespace Insurance.Api.Controllers.Models;

public record CartInsuranceDto(IEnumerable<InsuranceDto> CartItems, float InsuranceTotal);