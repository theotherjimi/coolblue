using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Insurance.Api.Controllers.Models;
using Insurance.Api.Insurance;
using Insurance.Api.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Insurance.Api.Controllers;

[Route("api/insurance")]
public class InsuranceController : Controller
{
    private readonly IInsuranceCalculator _insuranceCalculator;
    private readonly IProductRepository _productRepository;

    public InsuranceController(IProductRepository productRepository, IInsuranceCalculator insuranceCalculator)
    {
        _productRepository = productRepository;
        _insuranceCalculator = insuranceCalculator;
    }

    [HttpPost]
    [Route("product")]
    [ProducesResponseType(typeof(InsuranceDto), (int) HttpStatusCode.OK)]
    [ProducesResponseType((int) HttpStatusCode.NotFound)]
    public async Task<IActionResult> CalculateInsurance([FromBody] InsuranceRequestDto insuranceRequestDto)
    {
        var product = await _productRepository.GetProduct(insuranceRequestDto.ProductId);
        if (product is null) return NotFound();
        return Ok(new InsuranceDto(insuranceRequestDto.ProductId,
            _insuranceCalculator.CalculateInsuranceForProduct(product, 1)));
    }

    [HttpPost]
    [Route("cart")]
    [ProducesResponseType(typeof(CartInsuranceDto), (int) HttpStatusCode.OK)]
    public async Task<IActionResult> CalculateCartInsurance([FromBody] CartInsuranceRequestDto cartInsuranceRequestDto)
    {
        var products =
            (await _productRepository.GetProducts(cartInsuranceRequestDto.CartItems.Select(c => c.ProductId)))
            .ToList();

        var cartInsurance = cartInsuranceRequestDto.CartItems
            .Select(cartItem =>
            {
                var product = products.FirstOrDefault(p => p.Id == cartItem.ProductId);
                if (product == null) return null;
                return new InsuranceDto(cartItem.ProductId,
                    _insuranceCalculator.CalculateInsuranceForProduct(product, cartItem.Count));
            })
            .Where(c => c != null)
            .Cast<InsuranceDto>()
            .ToList();
        return Ok(new CartInsuranceDto(cartInsurance, cartInsurance.Sum(i => i.InsuranceValue)));
    }
}