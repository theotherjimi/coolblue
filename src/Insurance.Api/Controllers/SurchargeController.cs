﻿using System.Net;
using System.Threading.Tasks;
using Insurance.Api.Repository;
using Insurance.Api.Repository.Models;
using Microsoft.AspNetCore.Mvc;

namespace Insurance.Api.Controllers;

[Route("api/surcharge")]
public class SurchargeController : Controller
{
    private readonly ISurchargeRepository _surchargeRepository;

    public SurchargeController(ISurchargeRepository surchargeRepository)
    {
        _surchargeRepository = surchargeRepository;
    }

    [HttpGet]
    [Route("{productTypeId:int}")]
    [ProducesResponseType((int) HttpStatusCode.NotFound)]
    public async Task<IActionResult> GetProductTypeSurcharge(int productTypeId)
    {
        var surcharge = await _surchargeRepository.GetSurcharge(productTypeId);
        if (surcharge == null) return NotFound();
        return Ok(surcharge);
    }

    [HttpPut]
    [Route("")]
    [ProducesResponseType((int) HttpStatusCode.Conflict)]
    public async Task<IActionResult> SetProductTypeSurcharge([FromBody] ProductTypeSurcharge productTypeSurcharge)
    {
        try
        {
            await _surchargeRepository.UpdateSurcharge(productTypeSurcharge);
        }
        catch (OptimisticConcurrencyException e)
        {
            ModelState.AddModelError("Version", e.Message);
            return Conflict(new ValidationProblemDetails(ModelState));
        }

        return Ok();
    }
}