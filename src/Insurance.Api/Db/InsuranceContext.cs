﻿using Microsoft.EntityFrameworkCore;

namespace Insurance.Api.Db;

public class InsuranceContext : DbContext
{
#pragma warning disable CS8618
    public InsuranceContext(DbContextOptions<InsuranceContext> options)
        : base(options)
    {
    }

    public DbSet<ProductTypeSurchargeEntity> ProductTypeSurcharges { get; set; }
#pragma warning restore CS8618
}