﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Insurance.Api.Db;

[Table("ProductTypeSurcharges")]
public class ProductTypeSurchargeEntity
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int ProductTypeId { get; set; }

    public int Surcharge { get; set; }

    [Timestamp] public byte[] RowVersion { get; set; }
}