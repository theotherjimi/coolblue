﻿using Insurance.Api.Repository.Models;

namespace Insurance.Api.Insurance;

public interface IInsuranceCalculator
{
    float CalculateInsuranceForProduct(Product product, int count);
}