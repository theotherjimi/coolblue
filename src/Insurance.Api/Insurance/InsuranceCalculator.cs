﻿using Insurance.Api.Repository.Models;

namespace Insurance.Api.Insurance;

public class InsuranceCalculator : IInsuranceCalculator
{
    public float CalculateInsuranceForProduct(Product product, int count)
    {
        if (product.ProductType is {CanBeInsured: false}) return 0;
        float insuranceValue = product.SalesPrice switch
        {
            >= 500 and < 2000 => 1000,
            >= 2000 => 2000,
            _ => 0
        };

        if (product.ProductType?.Name is "Laptops" or "Smartphones" or "Digital cameras") insuranceValue += 500;

        insuranceValue += product.ProductType?.SurchargeAmount ?? 0.0f;

        return insuranceValue * count;
    }
}